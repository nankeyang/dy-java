package com.dyj.applet.domain;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-29 16:39
 **/
public class SpuDetail {

    /**
     * 商品亮点标签
     */
    private List<SpuHighlight> highlights;

    /**
     * SPU图片，预售券必传
     */
    private List<String> image_list;

    /**
     *小程序结算类型 1-包销 2-代销
     */
    private Integer mp_settle_type;
    /**
     * 价格，单位分
     */
    private Long price;

    /**
     * 排序权重，按降序排列
     */
    private Long sort_weight;
    /**
     * 接入方商品ID
     */
    private String spu_ext_id;
    /**
     * spu_id
     */
    private String spu_id;
    /**
     * 入口信息
     */
    private SpuEntryInfo entry_info;

    /**
     * 商品信息
     */
    private String name;

    /**
     * spu类型号，1-日历房，30-酒店民宿预售券，90-门票，91-团购券
     */
    private Integer spu_type;
    /**
     *  状态：0-审核中，1-上架，2-下架，3-审核拒绝 4-审核通过 (对于spu_detail只存在上架和下架状态，对于spu_draft只存在 审核中，审核拒绝，审核通过这三种状态)
     */
    private Integer status;

    /**
     * 状态描述
     */
    private String status_desc;

    /**
     * 前台品类标签
     */
    private List<String> front_category_tag;
    /**
     * 价格，单位分
     */
    private Long origin_price;

    private SpuFilterStatus spu_filter_status;
    /**
     * 库存
     */
    private Long stock;
    /**
     * 接入方店铺ID列表
     */
    private List<String> supplier_ext_id_list;
    /**
     * 软件服务费率，万分数
     */
    private String take_rate;

    /**
     * SPU属性字段
     */
    private JSONObject attribute;

    public List<SpuHighlight> getHighlights() {
        return highlights;
    }

    public void setHighlights(List<SpuHighlight> highlights) {
        this.highlights = highlights;
    }

    public List<String> getImage_list() {
        return image_list;
    }

    public void setImage_list(List<String> image_list) {
        this.image_list = image_list;
    }

    public Integer getMp_settle_type() {
        return mp_settle_type;
    }

    public void setMp_settle_type(Integer mp_settle_type) {
        this.mp_settle_type = mp_settle_type;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getSort_weight() {
        return sort_weight;
    }

    public void setSort_weight(Long sort_weight) {
        this.sort_weight = sort_weight;
    }

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }

    public String getSpu_id() {
        return spu_id;
    }

    public void setSpu_id(String spu_id) {
        this.spu_id = spu_id;
    }

    public SpuEntryInfo getEntry_info() {
        return entry_info;
    }

    public void setEntry_info(SpuEntryInfo entry_info) {
        this.entry_info = entry_info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSpu_type() {
        return spu_type;
    }

    public void setSpu_type(Integer spu_type) {
        this.spu_type = spu_type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatus_desc() {
        return status_desc;
    }

    public void setStatus_desc(String status_desc) {
        this.status_desc = status_desc;
    }

    public List<String> getFront_category_tag() {
        return front_category_tag;
    }

    public void setFront_category_tag(List<String> front_category_tag) {
        this.front_category_tag = front_category_tag;
    }

    public Long getOrigin_price() {
        return origin_price;
    }

    public void setOrigin_price(Long origin_price) {
        this.origin_price = origin_price;
    }

    public SpuFilterStatus getSpu_filter_status() {
        return spu_filter_status;
    }

    public void setSpu_filter_status(SpuFilterStatus spu_filter_status) {
        this.spu_filter_status = spu_filter_status;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public List<String> getSupplier_ext_id_list() {
        return supplier_ext_id_list;
    }

    public void setSupplier_ext_id_list(List<String> supplier_ext_id_list) {
        this.supplier_ext_id_list = supplier_ext_id_list;
    }

    public String getTake_rate() {
        return take_rate;
    }

    public void setTake_rate(String take_rate) {
        this.take_rate = take_rate;
    }

    public JSONObject getAttribute() {
        return attribute;
    }

    public void setAttribute(JSONObject attribute) {
        this.attribute = attribute;
    }
}
