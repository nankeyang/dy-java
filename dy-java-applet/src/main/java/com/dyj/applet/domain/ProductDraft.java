package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 11:35
 **/
public class ProductDraft {
    /**
     * 草稿状态
     * 10-审核中
     * 12-审核失败
     * 1-审核通过
     * 11-审核通过（老库存量数据,新接口保存后擦除）
     */
    private Integer draft_status;

    /**
     * 审核失败原因
     */
    private String audit_msg;

    private ProductStruct product;

    private SkuStruct sku;

    public Integer getDraft_status() {
        return draft_status;
    }

    public void setDraft_status(Integer draft_status) {
        this.draft_status = draft_status;
    }

    public String getAudit_msg() {
        return audit_msg;
    }

    public void setAudit_msg(String audit_msg) {
        this.audit_msg = audit_msg;
    }

    public ProductStruct getProduct() {
        return product;
    }

    public void setProduct(ProductStruct product) {
        this.product = product;
    }

    public SkuStruct getSku() {
        return sku;
    }

    public void setSku(SkuStruct sku) {
        this.sku = sku;
    }
}
