package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 17:33
 **/
public class CommonPlanTalent {

    /**
     * 达人带货场景
     * 1-仅短视频
     * 2-仅直播间
     * 3-短视频和直播间均有
     */
    private Integer content_type;
    /**
     *达人抖音号
     */
    private String douyin_id;
    /**
     * 达人带货GMV
     */
    private Long gmv;
    /**
     * 达人抖音昵称
     */
    private String nickname;

    public Integer getContent_type() {
        return content_type;
    }

    public void setContent_type(Integer content_type) {
        this.content_type = content_type;
    }

    public String getDouyin_id() {
        return douyin_id;
    }

    public void setDouyin_id(String douyin_id) {
        this.douyin_id = douyin_id;
    }

    public Long getGmv() {
        return gmv;
    }

    public void setGmv(Long gmv) {
        this.gmv = gmv;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
