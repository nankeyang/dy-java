package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 13:39
 **/
public class BusinessLicense {

    /**
     * 服务上营业执照公司名称
     */
    private String company;
    /**
     * 商家营业执照外部id
     */
    private String ext_id;
    /**
     * 商家营业执照链接
     */
    private String url;

    public static BusinessLicenseBuilder builder() {
        return new BusinessLicenseBuilder();
    }

    public static class BusinessLicenseBuilder {
        private String company;
        private String extId;
        private String url;

        public BusinessLicenseBuilder company(String company) {
            this.company = company;
            return this;
        }
        public BusinessLicenseBuilder extId(String extId) {
            this.extId = extId;
            return this;
        }
        public BusinessLicenseBuilder url(String url) {
            this.url = url;
            return this;
        }
        public BusinessLicense build() {
            BusinessLicense businessLicense = new BusinessLicense();
            businessLicense.setCompany(company);
            businessLicense.setExt_id(extId);
            businessLicense.setUrl(url);
            return businessLicense;
        }
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getExt_id() {
        return ext_id;
    }

    public void setExt_id(String ext_id) {
        this.ext_id = ext_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
