package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.PoiServiceBaseData;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 15:43
 **/
public class PoiServiceBaseDataVo extends BaseVo {

    private List<PoiServiceBaseData> result_list;

    public List<PoiServiceBaseData> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<PoiServiceBaseData> result_list) {
        this.result_list = result_list;
    }
}
