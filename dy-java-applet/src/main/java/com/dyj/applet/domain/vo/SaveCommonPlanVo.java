package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-05-13 16:13
 **/
public class SaveCommonPlanVo {

    /**
     * 计划ID
     */
    private Long plan_id;

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }
}
