package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SpuDetail;
import com.dyj.applet.domain.SpuDraft;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-29 16:38
 **/
public class SpuVo extends BaseVo {

    private SpuDetail spu_detail;


    private List<SpuDraft> spu_draft;

    public SpuDetail getSpu_detail() {
        return spu_detail;
    }

    public void setSpu_detail(SpuDetail spu_detail) {
        this.spu_detail = spu_detail;
    }

    public List<SpuDraft> getSpu_draft() {
        return spu_draft;
    }

    public void setSpu_draft(List<SpuDraft> spu_draft) {
        this.spu_draft = spu_draft;
    }
}
