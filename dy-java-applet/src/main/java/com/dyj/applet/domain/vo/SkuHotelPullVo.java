package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-29 14:45
 **/
public class SkuHotelPullVo extends BaseVo {

    /**
     * 房型价格(人民币分)
     */
    private Long price;
    /**
     * 接入方SPU ID
     */
    private String spu_ext_id;
    /**
     * 价格时间区间[start_date, end_date)
     */
    private String start_date;

    /**
     * 价格时间区间[start_date, end_date)
     */
    private String end_date;
    /**
     * 	库存量
     */
    private Long stock;

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
