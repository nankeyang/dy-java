package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 15:16
 **/
public class SupplierPoiInfo {
    /**
     * 一级品类
     */
    private String first_category;
    /**
     * 二级品类
     */
    private String second_category;
    /**
     * 三级品类
     */
    private String third_category;

    public String getFirst_category() {
        return first_category;
    }

    public void setFirst_category(String first_category) {
        this.first_category = first_category;
    }

    public String getSecond_category() {
        return second_category;
    }

    public void setSecond_category(String second_category) {
        this.second_category = second_category;
    }

    public String getThird_category() {
        return third_category;
    }

    public void setThird_category(String third_category) {
        this.third_category = third_category;
    }
}
